<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'polpriv_description' => 'Ce plugin ajoute dans les préférences de l\'utilisateur une option permettant de modifier la police de caractères de utilisée dans l\'espace privé.',
	'polpriv_slogan' => 'Personnalisez la police de caractères de l\'espace privé',
);
?>