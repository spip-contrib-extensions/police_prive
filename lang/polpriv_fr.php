<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'label_police_prive' => 'Police de caractères',
	'label_police_fontface_prive' => 'Les familles de polices @font-face placées dans le répertoire <em>&laquo;squelettes/polices&raquo;</em> apparaîtront en fin de liste.<br> Elles doivent disposer de 3 types de fichiers pour être prises en compte : .ttf, .wott et .eot.',
);
?>
